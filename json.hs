module Json (JsonValue, parseJson) where

import Data.Map (Map, toList, fromList)
import qualified Data.Text.Lazy.Read as Read 
import Data.Text.Lazy (pack)
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Language
import Text.ParserCombinators.Parsec.Token
import Char (chr)

data JsonValue = JsonNull | 
                 JsonTrue | 
                 JsonFalse | 
                 JsonNumber Double | 
                 JsonString String | 
                 JsonArray [JsonValue] | 
                 JsonObject (Data.Map.Map String JsonValue)

showContent with [] = ""
showContent with (x:[]) = with x
showContent with (x:xs) = with x ++ "," ++ showContent with xs 

instance Show JsonValue where
    show JsonNull = "null" 
    show JsonTrue = "true"
    show JsonFalse = "false"
    show (JsonNumber x) = show x
    show (JsonString s) = "\"" ++ escape s ++ "\""
    show (JsonArray l) = "[" ++ showContent show l ++ "]"
    show (JsonObject m) = "{" ++ showContent mapPair (Data.Map.toList m) ++ "}"
        where mapPair (k, v) = "\"" ++ escape k  ++ "\":" ++ (show v) 

direct :: String -> JsonValue -> GenParser Char st JsonValue
direct s v = do { string s
                ; return v
                }

jsnull = direct "null" JsonNull
jstrue = direct "true" JsonTrue
jsfalse = direct "false" JsonFalse

positiveNumber = do { f <- naturalOrFloat $ makeTokenParser emptyDef
                    ; return $ either fromIntegral id f
                    }
negativeNumber = do { char '-'
                    ; n <- positiveNumber
                    ; return (-n)
                    }

jsnumber = do { n <- (negativeNumber <|> positiveNumber)
              ; return (JsonNumber n)
              }

escape [] = []
escape (c:cs) = escapeChar c ++ escape cs
    where escapeChar c = case c of 
                              '\"' -> "\\\"" 
                              '\\' -> "\\\\"
                              '/' -> "\\/"
                              '\b' -> "\\b"
                              '\f' -> "\\f"
                              '\n' -> "\\n"
                              '\r' -> "\\t"
                              _ -> [c]

escapedChar = do { char '\\'
                 ; escaped
                 }
    where escaped = char '\"' <|> char '\\' <|> char '/' <|> 
                    char '\b' <|> char '\f' <|> char '\n' <|> 
                    char '\r' <|> char '\t'

unicodeChar = do { string "\\u"
                 ; us <- count 4 hexDigit
                 ; return $ either error (chr.fst) (Read.hexadecimal $ pack us)
                 }

jschar = try escapedChar <|> unicodeChar <|> noneOf "\"\\"

commonstring = do { char '\"'
                  ; manyTill jschar (char '\"')
                  }

jsstring = do { s <- commonstring
              ; return (JsonString s)
              }

jsarray = do { vs <- between (char '[') (char ']') elements 
             ; return (JsonArray vs)
             }

itemSep = do { spaces; (char ','); spaces } 

elements = sepBy value itemSep

jsobject = do { kvs <- between (char '{') (char '}') members 
              ; return (JsonObject $ fromList kvs)
              }
members = sepBy pair itemSep 
pair = do { k <- commonstring 
          ; spaces
          ; char ':'
          ; spaces
          ; v <- value
          ; return (k, v)
          }

value = jsobject <|> 
        jsarray <|>
        jsstring <|>
        jsnumber <|>
        jstrue <|>
        jsfalse <|>
        jsnull

jsonparser = do { spaces; v <- value; spaces; return v }

parseJson = parse jsonparser ""
